﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBRowi
{
    class incident
    {
        public int Id { get; set; }
        public DateTime? IncidentTime { get; set; }
        public Decision decision { get; set; }
        public string Description { get; set; }
        public ICollection<People> suspects { get; set; } = new List<People>();
        public ICollection<People> Guilty { get; set; } = new List<People>();
    }
}
