﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBRowi
{
    class People
    {
        public int Id { get; set; }
        public string FIO { get; set; }
        public string Address { get; set; }
        public int CriminalRecord { get; set; }
        public ICollection<incident> incidents { get; set; } = new List<incident>();
    }
}
